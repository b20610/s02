import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] arrFruits = {"apple","avocado","banana","kiwi","orange"};

        Scanner fruitScanner = new Scanner(System.in);
        System.out.println("Fruits in stock: " + Arrays.toString(arrFruits));
        System.out.println("Which fruit would you like to get the index of?");
        String searchFruit = fruitScanner.nextLine();
        int result = Arrays.binarySearch(arrFruits,searchFruit);
        System.out.println("The index of " + searchFruit + " is: " + result);

        ArrayList<String> friends = new ArrayList<>();

        friends.add("Lisa");
        friends.add("Jennie");
        friends.add("Jisoo Noona");
        friends.add("Rose");

        System.out.println("My friends are: " + friends);

        HashMap<String,Integer> inventory = new HashMap<>();

        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);

    }
}